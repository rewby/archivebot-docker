#!/usr/bin/env bash

while true
  do
    COUNT=$(find "$1" -type f |wc -l)
    if [ $COUNT -ne 0 ] ; then
      echo "Directory $1 is not empty ($COUNT files remaining). Waiting 5 seconds..."
      sleep 5
    else 
      echo "Directory $1 is empty!"
      break
    fi 
 done

