#!/usr/bin/env bash
set -euo pipefail

INPUT="$1"
STAGING="$2"
OUTPUT="$3"

while true ; do
  echo "[Stager] Attempting to move files..."
COUNT=$(find "$INPUT" -type f | wc -l)
  if [[ $COUNT -ne 0 ]]; then
    echo "[Stager] Found $COUNT files!"
    echo "[Stager] Moving from input to staging..."
    mv -v "$INPUT"/* "$STAGING/" || true
    echo "[Stager] Atomically moving from staging to output..."
    mv -v "$STAGING"/* "$OUTPUT/" || true
  fi
  sleep 30
done

